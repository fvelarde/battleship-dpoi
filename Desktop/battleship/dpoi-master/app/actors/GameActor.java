package actors;

import actors.messages.GameMssg;
import actors.messages.GameMssg.PlayerMssg;
import actors.messages.GameMssg.SetShip;
import actors.messages.GameMssg.CreateGame;
import actors.messages.GameMssg.Shoot;
import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import model.FinishedGameStatus;
import model.Game;
import model.GameBoard;
import model.Player;
import model.ships.HitResult;
import org.jetbrains.annotations.Nullable;
import play.libs.Json;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static model.FinishedGameStatus.OPPONENT_LEFT;
import static model.FinishedGameStatus.WIN;

/**
 * Created by tomasnajun on 20/06/16.
 */
public class GameActor extends AbstractActor {

    private Game game;
    private Cancellable cancellable;
    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private HashSet<String> playerReadyToPlay = new HashSet<>();

    private final HashMap<ActorRef, GameBoard> gameBoards = new HashMap<>();
    private Player userPlaying = null;
    private Cancellable turnTimeExpired;

    public GameActor() {
        receive(ReceiveBuilder
                .match(CreateGame.class, createGame -> {
                    //TODO ver si puedo instanciarlo en el constructor
                    final Game game = createGame(createGame);
                    if (game == null) {
                        close();
                        return;
                    }

                    this.game = game;
                    final GameMssg.GameCreated gameCreated = new GameMssg.GameCreated(self().path().name());
                    gameBoards.keySet().forEach(actorRef -> actorRef.tell(gameCreated, self()));
                })
                .match(GameMssg.Ready.class, ready -> {
                    playerReadyToPlay.add(sender().path().name());
                    if (playerReadyToPlay.size() > 1 && userPlaying == null) {
                        changeTurn();
                    }
                })
                .match(SetShip.class, setShip -> {
                    final GameBoard gameBoard = gameBoards.get(sender());
                    gameBoard.setShip(setShip.size, setShip.row, setShip.col);
                })
                .match(Shoot.class, shoot -> {
                    resetTurnTimer();
                    final GameBoard playerBoard = gameBoards.get(sender());
                    final ActorRef opponentRef = getOpponentRef(sender());
                    if (opponentRef == null) return;
                    if (!isYourTurn(playerBoard)) return;

                    final GameBoard opponentGameBoard = gameBoards.get(opponentRef);
                    final HitResult hitResult = opponentGameBoard.receiveShoot(shoot.row, shoot.col);

                    final GameMssg.ShootResult shootResult;
                    final GameMssg.ReceiveShoot receiveShoot;
                    if (won(hitResult)) {
                        receiveShoot = new GameMssg.ReceiveShoot(shoot.row, shoot.col, HitResult.LOOSE);
                        shootResult = new GameMssg.ShootResult(shoot.row, shoot.col, HitResult.WIN);
                    } else {
                        receiveShoot = new GameMssg.ReceiveShoot(shoot.row, shoot.col, hitResult);
                        shootResult = new GameMssg.ShootResult(shoot.row, shoot.col, hitResult);
                    }
                    opponentRef.tell(receiveShoot, self());
                    sender().tell(shootResult, self());
                    playerBoard.annotate(shoot.row, shoot.col, hitResult);
                    if (!won(hitResult)) changeTurn();
                    else finishGame();
                })
                .match(GameMssg.LeaveGame.class, leaveGame -> {
                    resetTurnTimer();
                    final ActorRef opponentRef = getOpponentRef(sender());
                    if (opponentRef == null) {
                        log.error("LeaveGame: opponentRef is null!");
                        return;
                    }
                    final GameMssg.EndGame endGame = new GameMssg.EndGame(OPPONENT_LEFT);
                    opponentRef.tell(endGame, self());

                    finishGame();
                })
                .match(GameMssg.PlayerDisconnected.class, disconnectionMessage -> {
                    final ActorRef actorRef = disconnectionMessage.playerDisconnected;
//                    final GameBoard gameBoard = gameBoards.get(actorRef);
//                    final Player playerDisconnected = gameBoard.getOwner();

                    final ActorSystem system = context().system();
                    cancellable = system.scheduler().scheduleOnce(Duration.create(3000, TimeUnit.SECONDS),
                            self(), new GameMssg.LeaveGame(null), system.dispatcher(), actorRef);
                })
                .match(GameMssg.ContinueGame.class, continueGame -> {
                    final PlayerMssg player = continueGame.player1;
                    if (cancellable == null) return;
                    if (!cancellable.cancel()) return;

                    final GameBoard gameBoard = gameBoards.remove(continueGame.oldActorRef);
                    gameBoards.put(player.actorRef, gameBoard);
                    final ActorRef opponentRef = getOpponentRef(player.actorRef);
                    if (opponentRef != null) {
                        opponentRef.tell(new GameMssg.ContinueGame(null, null), self());
                    } else log.info("Opponent Ref is Null");
                    sender().tell(new GameMssg.ReloadGame(gameBoard), self());
                })
                .match(GameMssg.TimeExpired.class, ignored -> {
                    sender().tell(new GameMssg.TimeExpired(), self());
                    changeTurn();
                })
                .build());
    }

    private void resetTurnTimer() {
        if (turnTimeExpired != null) {
            turnTimeExpired.cancel();
        }
    }

    private boolean won(HitResult hitResult) {
        return hitResult.name().equals(HitResult.WIN.name());
    }

    private void finishGame() {
        saveGameState();
        final GameBoard gameBoard = gameBoards.get(sender());
        gameBoard.getOwner().addWonGame(game);
        final GameBoard opponentBoard = gameBoards.get(getOpponentRef(sender()));
        opponentBoard.getOwner().addLostGame(game);
        close();
    }

    private boolean isYourTurn(GameBoard playerBoard) {
        return playerBoard.getOwner().getId() == userPlaying.getId();
    }

    private void close() {
        self().tell(PoisonPill.getInstance(), self());
    }

    @Nullable
    private Game createGame(CreateGame createGame) {
        final PlayerMssg playerActor1 = createGame.player1;
        final PlayerMssg playerActor2 = createGame.player2;

        final Player player1 = Player.find.byId(playerActor1.playerDBId);
        final Player player2 = Player.find.byId(playerActor2.playerDBId);
        if (player1 == null || player2 == null) return null;
        final Game game = new Game(player1, player2);
        game.save();

        final GameBoard gameBoard1 = game.getPlayerGameBoard(player1);
        final GameBoard gameBoard2 = game.getPlayerGameBoard(player2);
        gameBoards.put(playerActor1.actorRef, gameBoard1);
        gameBoards.put(playerActor2.actorRef, gameBoard2);
        return game;
    }

    private void changeTurn() {
        final ActorRef opponentRef = getOpponentRef(sender());
        if (opponentRef != null) {
            final GameBoard opponentGameBoard = gameBoards.get(opponentRef);
            userPlaying = opponentGameBoard.getOwner();
            opponentRef.tell(new GameMssg.YourTurn(), self());
            final ActorSystem system = context().system();
            turnTimeExpired = system.scheduler().scheduleOnce(Duration.create(30, TimeUnit.SECONDS),
                    self(), new GameMssg.TimeExpired(), system.dispatcher(), opponentRef);
        } else log.error("ChangeTurn: opponentRef is null!");
    }

    @Nullable
    private ActorRef getOpponentRef(ActorRef sender) {
        for (final Map.Entry<ActorRef, GameBoard> entry : gameBoards.entrySet()) {
            final ActorRef opponentRef = entry.getKey();
            if (!opponentRef.equals(sender)) {
                return opponentRef;
            }
        }
        return null;
    }

    private void saveGameState() {
        game.save();
    }
}
