package actors;

import actors.messages.GameMssg;
import actors.messages.GameMssg.ContinueGame;
import actors.messages.GameMssg.CreateGame;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by tomasnajun on 20/06/16.
 */
public class GamesActor extends UntypedActor {
    /**
     * <PlayerActorRef, PlayerId>
     */
    private final Map<ActorRef, Long> allPlayers = new HashMap<>();
    /**
     * <PlayerId, PlayerActorRef>
     */
    private final Map<Long, ActorRef> playersInGame = new HashMap<>(); //TODO es necesario que sean mapa?
    /**
     * <PlayerId, PlayerActorRef>
     */
    private final Map<Long, ActorRef> waitingPlayers = new HashMap<>(); //TODO es necesario que sean mapa?
    /**
     * <PlayerId, GameActorRef>
     */
    private final Map<Long, ActorRef> games = new HashMap<>();

    private static final String GAME = "game-";
    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof GameMssg.JoinGame) {
            final GameMssg.JoinGame joinGame = (GameMssg.JoinGame) message;
            final GameMssg.PlayerMssg player1 = joinGame.waitingPlayer;
            final long player1Id = player1.playerDBId;
            final ActorRef player1ActorRef = player1.actorRef;

            allPlayers.put(player1ActorRef, player1Id);

            if (playersInGame.containsKey(player1Id)) {
                reconnectPlayer(player1);
            } else {
                //create new Game
                if (waitingPlayers.isEmpty()) {
                    waitingPlayers.put(player1Id, player1ActorRef);
                    log.info("usuario Agregado");
                } else {
                    if (waitingPlayers.containsKey(player1Id)) {
                        waitingPlayers.replace(player1Id, player1ActorRef);
                    }
                    final Iterator<Map.Entry<Long, ActorRef>> iterator = waitingPlayers.entrySet().iterator();
                    Map.Entry<Long, ActorRef> next = iterator.next();
                    if (itIsMe(player1Id, iterator, next))  next = iterator.next();

                    createGame(player1, next);
                }
            }
            return;
        }
        if (message instanceof GameMssg.Ready) {
            final GameMssg.Ready ready = (GameMssg.Ready) message;
            final String gameName = ready.gameName;
            getContext().getChild(gameName).forward(ready, context());
            return;
        }

        if (message instanceof GameMssg.SetShip) {
            final GameMssg.SetShip setShip = (GameMssg.SetShip) message;
            final String gameName = setShip.gameName;
            getContext().getChild(gameName).forward(setShip, context());
            return;
        }

        if (message instanceof GameMssg.Shoot) {
            final GameMssg.Shoot shoot = (GameMssg.Shoot) message;
            final String gameName = shoot.gameName;
            getContext().getChild(gameName).forward(shoot, context());
            return;
        }

        if (message instanceof GameMssg.LeaveGame) {
            final GameMssg.LeaveGame leaveGame = (GameMssg.LeaveGame) message;
            final String gameName = leaveGame.gameName;
            getContext().getChild(gameName).forward(leaveGame, context());

            leaveGame.gameName()
                    .map(getContext()::getChild)
                    .<Iterable<ActorRef>>map(Collections::singletonList);
            return;
        }

        if (message instanceof GameMssg.PlayerDisconnected) {
            final Long playerId = allPlayers.get(sender());
            log.info("Player disconnected id: " + playerId);
            if (playerId == null) {
                log.info("Player Disconnect playerId null");
                return;
            }
            final ActorRef gameActorRef = games.get(playerId);
            if (gameActorRef == null) {
                log.info("GameActor is null");
                return;
            }
            gameActorRef.tell(new GameMssg.PlayerDisconnected(gameActorRef), self());
            return;
        }

        if (message instanceof Terminated) {
            final Terminated terminated = (Terminated) message;
            final ActorRef gameActorTerminated = terminated.actor();
            for (final Map.Entry<Long, ActorRef> entry : games.entrySet()) {
                final ActorRef gameActor = entry.getValue();
                if (!gameActor.equals(gameActorTerminated)) continue;
                final Long playerId = entry.getKey();
                final ActorRef removedActor = playersInGame.remove(playerId);
                allPlayers.remove(removedActor);
            }
            return;
        }

        unhandled(message);
    }

    private boolean itIsMe(long player1Id, Iterator<Map.Entry<Long, ActorRef>> iterator, Map.Entry<Long, ActorRef> next) {
        return next.getKey().equals(player1Id) && iterator.hasNext();
    }

    private void reconnectPlayer(GameMssg.PlayerMssg player1) {
        final long player1Id = player1.playerDBId;
        final ActorRef oldActorRef = playersInGame.replace(player1Id, player1.actorRef);
        if (!playerIsInAGame(player1Id)) return;
        final ActorRef game = games.get(player1Id);
        game.forward(new ContinueGame(player1, oldActorRef), context());
        log.info("Usuario reconectado a juego");
    }

    private void createGame(GameMssg.PlayerMssg player1, Map.Entry<Long, ActorRef> next) {
        long player1Id = player1.playerDBId;
        final GameMssg.PlayerMssg player2 = new GameMssg.PlayerMssg(next.getValue(), next.getKey());
        final long player2Id = player2.playerDBId;
        //Create GameActor
        final String gameName = GAME + player1Id + player2Id;
        final Props props = Props.create(GameActor.class);
        final ActorRef gameChild = context().actorOf(props, gameName);
        final CreateGame createGame = new CreateGame(player1, player2);
        gameChild.forward(createGame, context());

        this.getContext().watch(gameChild);

        games.put(player1Id, gameChild);
        games.put(player2Id, gameChild);
        playersInGame.put(player1Id, player1.actorRef);
        playersInGame.put(player2Id, player2.actorRef);
        waitingPlayers.remove(player2Id);
        log.info("Game creado");
    }

    private boolean playerIsInAGame(long player1Id) {
        return games.containsKey(player1Id);
    }
}
