package actors;

import actors.messages.GameMssg;
import actors.messages.ResponseFactory;
import akka.actor.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.assistedinject.Assisted;
import model.GameBoard;
import play.Configuration;

import javax.inject.Inject;
import javax.inject.Named;

import static actors.messages.ResponseFactory.*;
import static play.mvc.Controller.session;

/**
 * Created by tomasnajun on 15/06/16.
 */
public class PlayerActor extends UntypedActor{
    private final ActorRef out;
    private final ActorRef gamesActor;
    private Configuration configuration;
    private final long playerId;

    @Inject
    public PlayerActor(@Assisted ActorRef out,
                       @Assisted long playerId,
                       @Named("gamesActor") ActorRef gamesActor,
                       Configuration configuration) {
        this.out = out;
        this.playerId = playerId;
        this.gamesActor = gamesActor;
        this.configuration = configuration;
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
    }

    @Override
    public void onReceive(Object msg) throws Exception {

        if (msg instanceof GameMssg.GameCreated) {
            final GameMssg.GameCreated gameCreated = (GameMssg.GameCreated) msg;
            final JsonNode gameCreatedMessage = ResponseFactory.gameCreated(gameCreated.gameName);
            out.tell(gameCreatedMessage, self());
            return;
        }

        if (msg instanceof GameMssg.YourTurn) {
            final JsonNode yourTurn = ResponseFactory.yourTurn();
            out.tell(yourTurn, self());
            return;
        }

        if (msg instanceof GameMssg.ShootResult) {
            final GameMssg.ShootResult shootResult = (GameMssg.ShootResult) msg;
            final JsonNode shootResultMessage = shootResult(shootResult.row, shootResult.col, shootResult.hitResult);
            out.tell(shootResultMessage, self());
            return;
        }

        if (msg instanceof GameMssg.ReceiveShoot) {
            final GameMssg.ReceiveShoot receiveShoot = (GameMssg.ReceiveShoot) msg;
            final JsonNode receiveShootMessage = receiveShoot(receiveShoot.row, receiveShoot.col, receiveShoot.hitResult);
            out.tell(receiveShootMessage, self());
            return;
        }

        if (msg instanceof GameMssg.EndGame) {
            final GameMssg.EndGame endGame = (GameMssg.EndGame) msg;
            final JsonNode endGameMessage = endGame(endGame.status);
            out.tell(endGameMessage, self());
            return;
        }

        if (msg instanceof GameMssg.ReloadGame) {
            final GameBoard gameBoard = ((GameMssg.ReloadGame) msg).gameBoard;
            final JsonNode continueGame = continueGame(gameBoard);
            out.tell(continueGame, self());
            return;
        }

        if (msg instanceof GameMssg.TimeExpired) {
            out.tell(ResponseFactory.opponentTurn(), self());
            return;
        }

        if (msg instanceof JsonNode) {
            final JsonNode json = (JsonNode) msg;
            final String type = json.get("type").textValue();
            String gameName = null;
            final JsonNode name = json.get("gameName");
            if (name != null) {
                gameName = name.textValue();
            }
            final ObjectMapper objectMapper = new ObjectMapper();

            switch (type) {
                case "joinGame":
                    final GameMssg.JoinGame joinGame = new GameMssg.JoinGame(new GameMssg.PlayerMssg(self(), playerId));
                    gamesActor.tell(joinGame, self());
                    System.out.println(joinGame);
                    break;
                case "setShip":
                    Integer[] rows = objectMapper.readValue(json.get("row").toString(), Integer[].class);
                    Integer[] cols = objectMapper.readValue(json.get("col").toString(), Integer[].class);
                    final int len = json.get("len").asInt();
                    final GameMssg.SetShip setShip = new GameMssg.SetShip(cols, rows, len, gameName);
                    gamesActor.tell(setShip, self());
                    System.out.println("setShip = " + setShip);
                    break;
                case "shoot":
                    final int row = json.get("row").asInt();
                    final int col = json.get("col").asInt();
                    final GameMssg.Shoot shoot = new GameMssg.Shoot(gameName, row, col);
                    gamesActor.tell(shoot, self());
                    System.out.println("shoot = " + shoot);
                    break;
                case "leaveGame":
                    final GameMssg.LeaveGame leaveGame = new GameMssg.LeaveGame(gameName);
                    gamesActor.tell(leaveGame, self());
                    System.out.println("leaveGame = " + leaveGame);
                    break;
                case "ready":
                    final GameMssg.Ready ready = new GameMssg.Ready(gameName);
                    gamesActor.tell(ready, self());
                    System.out.println("ready = " + ready);
                    break;
                default:
                    break;
            }
        }
    }

    public void close() {
        self().tell(PoisonPill.getInstance(), self());
    }

    public interface Factory {
        Actor create(ActorRef out, long playerId);
    }
}
