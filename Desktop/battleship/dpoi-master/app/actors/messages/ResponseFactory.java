package actors.messages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.FinishedGameStatus;
import model.Game;
import model.GameBoard;
import model.ships.HitResult;
import play.libs.Json;

/**
 * Created by tomasnajun on 21/06/16.
 */
public class ResponseFactory {

    private static final String TYPE = "type";

    private ResponseFactory() {
    }

    public static JsonNode gameCreated(String gameName) {
        return Json.newObject()
                .put(TYPE, "gameCreated")
                .put("gameName", gameName);
    }

    public static JsonNode startGame(String playerWhoPlayFacebookId) {
        return Json.newObject()
                .put(TYPE, "startGame")
                .put("playerDBId", playerWhoPlayFacebookId);
    }

    public static JsonNode receiveShoot(int row, int col, HitResult hitResult) {
        return shoot("receiveShoot", row, col, hitResult);
    }

    public static JsonNode shootResult(int row, int col, HitResult hitResult) {
        return shoot("shootResult", row, col, hitResult);
    }

    private static JsonNode shoot(String shootType, int row, int col, HitResult hitResult) {
        return Json.newObject()
                .put(TYPE, shootType)
                .put("row", row)
                .put("col", col)
                .put("result", hitResult.name().toLowerCase());
    }

    public static JsonNode yourTurn() {
        return Json.newObject()
                .put(TYPE, "yourTurn");
    }

    public static JsonNode endGame(FinishedGameStatus finishedGameStatus) {
        return Json.newObject()
                .put(TYPE, "endGame")
                .put("status", finishedGameStatus.name().toLowerCase());
    }

    public static JsonNode continueGame(GameBoard gameBoard) {
        final ObjectMapper mapper = Json.mapper().setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String jsonGameBoard = "";
        try {
            jsonGameBoard = mapper.writeValueAsString(gameBoard);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Json.newObject()
                .put(TYPE, "continueGame")
                .set("gameBoard", Json.parse(jsonGameBoard));
    }

    public static JsonNode opponentTurn() {
        return Json.newObject().put(TYPE, "opponentTurn");
    }
}
