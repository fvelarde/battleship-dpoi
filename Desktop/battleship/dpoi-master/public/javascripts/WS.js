/**
 * Created by tomasnajun on 16/07/16.
 */
var WS = {
    ws: null,
    
    game: null,

    openWS: function (url) {
        this.ws = new WebSocket(url);
        console.log("ws: " + this.ws);
    },
    
    sendMessage: function (message) {
        this.ws.send(JSON.stringify(message));
    },

    receiveMessages: function (event) {
        var message = JSON.parse(event.data);
        console.log(message);
        switch (message.type) {
            case "gameCreated":
                return this.game.gameCreated(message);
            case "shootResult":
                return this.game.shootResult(message);
            case "receiveShoot":
                return this.game.receiveShoot(message);
            // case "endGame":
            //     return this.game.endGame(message);
            case "yourTurn":
                return this.game.changeTurn(true);
            case "opponentTurn":
                return this.game.changeTurn(false);
            default:
                return console.log(message);
        }
    }
};
