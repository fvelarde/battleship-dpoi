/**
 * Created by tomasnajun on 16/07/16.
 */
var Game = {
    gameName: null,

    gameBoard: null,

    messenger: null,

    initGame: function (messenger, gameBoard) {
        this.messenger = messenger;
        this.gameBoard = gameBoard;
    },

    gameCreated: function (message) {
        this.gameName = message.gameName;
        document.getElementById("ready-button").style.display = 'block';
    },

    readyToPlay: function () {
        var ships = this.gameBoard.getShips();
        for (var i = 0; i < ships.length; i++) {
            this.setShip(ships[i]);
        }
        this.messenger.readyToPlay(this.gameName);
    },

    win: function () {
        alert("Congratulations: you won!");
        window.location.href = "/";
    },

    loose: function () {
        alert("Ooohh: you lost!");
        window.location.href = "/";
    },

    setShip: function (locationShip) {
        if (locationShip.length === 0) return;
        var rows = [];
        var columns = [];

        for (var j = 0; j < locationShip.length; j++) {
            var shipRow = Math.floor(locationShip[j] / 10) + 1;
            var shipCol = locationShip[j] % 10 + 1;
            rows.push(shipRow);
            columns.push(shipCol);
        }
        this.messenger.setShip(this.gameName, rows, columns, locationShip.length);
    },

    shootResult: function (message) {
        var result = message.result;
        const row = message.row;
        const col = message.col;
        var cellId = this.gameBoard.getOpponentCellId(row, col);
        switch (result) {
            case "sank":
                this.gameBoard.turnCellToSunken(row, col);
                //TODO que te avise que hundiste un barco ademas de pintarte
                break;
            case "hit":
                this.gameBoard.turnCellToFire(cellId);
                break;
            case "miss":
                this.gameBoard.turnCellToWater(cellId);
                break;
            case "win":
                this.gameBoard.turnCellToFire(cellId);
                this.win();
                break;
            default:
                this.gameBoard.turnCellToWater(cellId);
                break;
        }
    },

    changeTurn: function (isMyTurn) {
        if (isMyTurn) this.gameBoard.myTurn();
        else this.gameBoard.opponentTurn();
    },

    receiveShoot: function (message) {
        if (message.result === "sank") this.gameBoard.sinkShip(message.row, message.col);
        else this.gameBoard.receiveShoot(message.row, message.col);
        //todo handle loose state
        if (message.result === "loose") this.loose();
    },

    shoot: function (element) {
        const shoot = this.gameBoard.shoot(element);
        this.messenger.shoot(this.gameName, shoot.row, shoot.col);
    },

    leaveGame: function () {
        this.messenger.leaveGame(this.gameName);
        window.location.href = "/";
    }
};