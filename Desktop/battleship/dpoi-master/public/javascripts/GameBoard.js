/**
 * Created by tomasnajun on 16/07/16.
 */
var GameBoard = {
    board: [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ],
    rotateShip: function (shipId) {
        var ship = $('#' + shipId)[0];
        if (ship.className.match(/(?:^|\s)orient-v(?!\S)/)) {
            ship.className = ship.className.replace(/(?:^|\s)orient-v(?!\S)/g, ' ');
        } else {
            ship.className += " orient-v";
        }
    },
    turnCellToFire: function (cellId) {
        $(cellId).addClass("fired-cell");
    },
    turnCellToWater: function (cellId) {
        $(cellId).addClass("water-cell");
    },
    locateShip: function (column, row, orientation, length, id) {
        var cell = this.getCellNumber(row, column);
        if (orientation == "orient-v") {
            var last = cell + 10 * (length - 1);
            for (var j = cell; j <= last; j += 10) {
                this.board[j - 1] = id;
            }
        } else {
            for (var i = cell; i < (cell + length); i++) {
                this.board[i - 1] = id;
            }
        }
    },
    verifyCellsAvailability: function (column, row, orientation, length) {
        var cell = this.getCellNumber(row, column);
        if (orientation == "orient-v") {
            var last = cell + 10 * (length - 1);
            for (var i = cell; i <= last; i += 10) {
                if (this.board[i - 1] != 0) return false;
            }
        } else {
            for (var j = cell; j < (cell + length); j++) {
                if (this.board[j - 1] != 0) return false;
            }
        }
        return true;
    },
    receiveShoot: function (row, col) {
        var cellNumber = this.getCellNumber(row, col);
        var cellId = this.getMyCellId(row, col);
        console.log("receiveShoot -> cellId: " + cellId);
        if (this.board[cellNumber - 1] == 0) this.turnCellToWater(cellId);
        else this.turnCellToFire(cellId);
    },
    shoot: function (element) {
        var row = ((element.offsetTop) - 20 ) / 50;
        var column = ((element.offsetLeft - 15 ) / 50 ) + 1;

        element.removeAttribute("onclick");
        element.className = element.className.replace(/(?:^|\s)active-cell(?!\S)/g, ' ');

        this.opponentTurn(element);
        return {row: row, col: column};
    },
    myTurn: function () {
        document.getElementById("player-header").className += " active-player";
        if (document.getElementById("opponent-header").className.match(/(?:^|\s)active-player(?!\S)/)) {
            document.getElementById("opponent-header").className = document.getElementById("opponent-header").className.replace(/(?:^|\s)active-player(?!\S)/g, ' ');
        }

        var elements = document.getElementsByClassName("active-cell");
        for (var i = 0; i < elements.length; i++) {
            elements[i].setAttribute("onclick", "shoot(this)");
        }
    },
    opponentTurn: function () {
        var elements2 = document.getElementsByClassName("active-cell");
        for (var j = 0; j < elements2.length; j++) {
            elements2[j].removeAttribute("onclick")
        }

        document.getElementById("opponent-header").className += " active-player";
        document.getElementById("player-header").className = document.getElementById("opponent-header").className.replace(/(?:^|\s)active-player(?!\S)/g, ' ');
    },
    getShips: function () {
        var locationShip1 = [];
        var locationShip2 = [];
        var locationShip3 = [];
        var locationShip4 = [];
        var locationShip5 = [];
        var locationShip6 = [];
        var locationShip7 = [];
        var locationShip8 = [];
        for (var i = 0; i < this.board.length; i++) {
            var boardId = this.board[i];
            if (boardId == 0) continue;
            switch (boardId) {
                case "ship-1":
                    locationShip1.push(i);
                    break;
                case "ship-2":
                    locationShip2.push(i);
                    break;
                case "ship-3":
                    locationShip3.push(i);
                    break;
                case "ship-4":
                    locationShip4.push(i);
                    break;
                case "ship-5":
                    locationShip5.push(i);
                    break;
                case "ship-6":
                    locationShip6.push(i);
                    break;
                case "ship-7":
                    locationShip7.push(i);
                    break;
                case "ship-8":
                    locationShip8.push(i);
                    break;
                default:
                    break;
            }
        }
        return [locationShip1, locationShip2, locationShip3, locationShip4,
            locationShip5, locationShip6, locationShip7, locationShip8];
    },
    myDraggable: {
        grid: [50, 50], containment: ".containment-wrapper", scroll: false,
        stop: function () {
            var column = ( Math.floor(($(this).position().left) / 50) ) + 1;
            var row = Math.floor(($(this).position().top) / 50);
            var orientation;
            var length;
            var id = this.getAttribute("id");

            if (this.className.match(/(?:^|\s)orient-v(?!\S)/)) {
                orientation = "orient-v";
            }
            else {
                orientation = "orient-h";
            }

            if (this.className.match(/(?:^|\s)length-1(?!\S)/)) length = 1;
            else if (this.className.match(/(?:^|\s)length-2(?!\S)/)) length = 2;
            else length = 3;

            // var cell = (10 * ( row - 1)) + column;

            for (var i = 1; i <= 100; i++) {
                if (GameBoard.board[i] == id) GameBoard.board[i] = 0;
            }

            if (GameBoard.verifyCellsAvailability(column, row, orientation, length)) {
                if (document.getElementById(id).className.match(/(?:^|\s)wrong-location(?!\S)/)) {
                    document.getElementById(id).className = document.getElementById(id).className.replace(/(?:^|\s)wrong-location(?!\S)/g, '');
                }
                GameBoard.locateShip(column, row, orientation, length, id);
            }
            else {
                document.getElementById(id).className += " wrong-location";
                alert("Ops! You already have a ship on that position");
            }
        }
    },

    sinkShip: function (row, col) {
        const cellNumber = this.getCellNumber(row, col);
        console.log("cellNumber: " + cellNumber);
        const shipId = this.board[cellNumber - 1];
        console.log("shipId:" + shipId);
        for (var i = 0; i < this.board.length; i++) {
            const cellId = this.board[i];
            if (cellId === 0 || cellId != shipId) continue;
            console.log("cellId:" + cellId);
            var cell = "#cell-" + (i + 1);
            console.log("cell: " + cell);
            $(cell).addClass("sunken-cell");
        }
    },

    turnCellToSunken: function (row, col) {
        const cell = this.getOpponentCellId(row, col);
        $(cell)[0].className += " sunken-cell";
    },
    
    getCellNumber: function (row, col) {
        return (10 * ( row - 1)) + col;
    },
    
    getMyCellId: function (row, col) {
        const cellNumber = this.getCellNumber(row, col);
        return "#cell-" + cellNumber;
    },

    getOpponentCellId: function (row, col) {
        const cellNumber = this.getCellNumber(row, col);
        return "#cell-" + (cellNumber + 100);
    }
};