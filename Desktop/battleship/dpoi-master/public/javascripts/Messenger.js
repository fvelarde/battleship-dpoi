/**
 * Created by tomasnajun on 16/07/16.
 */
var Messenger = {
    myWs: null,
    initWS: function (wsInstance) {
        this.myWs = wsInstance;
    },
    joinGame: function () {
        var message = {"type": "joinGame"};
        this.myWs.sendMessage(message);
    },
    shoot: function (gameName, row, col) {
        var message = {"type" : "shoot", 
                        "gameName" : gameName,
                        "row" : row,
                        "col" : col};
        this.myWs.sendMessage(message);
    },
    readyToPlay: function (gameName) {
        var message = {"type" : "ready",
                        "gameName" : gameName};
        this.myWs.sendMessage(message);
    },
    setShip: function (gameName, rows, cols, length) {
        var message = {
            "type" : "setShip",
            "gameName" : gameName,
            "row" : rows,
            "col" : cols,
            "len" : length
        };
        this.myWs.sendMessage(message);
    },
    leaveGame: function (gameName) {
        var message = {
            "type" : "leaveGame",
            "gameName" : gameName
        };
        this.myWs.sendMessage(message);
    }
};