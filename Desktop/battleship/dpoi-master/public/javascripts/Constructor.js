/**
 * Created by tomasnajun on 17/07/16.
 */
var myWs, game, gameBoard;
$(function () {
    document.getElementById("ready-button").style.display = 'none';
    
    myWs = Object.create(WS);
    myWs.openWS($("body").data("ws-url"));
    var messenger = Object.create(Messenger);
    messenger.initWS(myWs);
    gameBoard = Object.create(GameBoard);
    game = Object.create(Game);
    game.initGame(messenger, gameBoard);
    
    myWs.game = game;
    
    myWs.ws.onopen = function () {
        messenger.joinGame();
    };
    myWs.ws.onmessage = function (event) {
        myWs.receiveMessages(event);
    };
    $(".draggable").draggable(gameBoard.myDraggable);
});

function leaveGame() {
    
}

function readyToPlay(element) {
    element.style.display = 'none';
    $(".draggable").draggable('disable');
    game.readyToPlay();
}

function rotateShip(shipId) {
    gameBoard.rotateShip(shipId);
}

function shoot(element) {
    game.shoot(element);
}

function suggestShot() {
    
}